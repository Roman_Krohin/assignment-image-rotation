#include "RotatePicture.h"
#include <stdlib.h> 

struct image createImage(uint16_t width, uint16_t height) {
    struct image img = {.width = width, .height = height};
    img.data = (struct pixel*)malloc(sizeof(struct pixel) * width * height);

    if (img.data == NULL) {
        exit(EXIT_FAILURE);
    }
    return img;
}

struct image rotate(const struct image source) {
    struct image rotated_img = createImage(source.height, source.width);

    for (uint16_t y = 0; y < source.height; y++) {
        for (uint16_t x = 0; x < source.width; x++) {
            uint16_t rotated_x = source.height - y - 1;
            uint16_t rotated_y = x;

            rotated_img.data[rotated_y * rotated_img.width + rotated_x] = source.data[y * source.width + x];
        }
    }

    return rotated_img;
}
