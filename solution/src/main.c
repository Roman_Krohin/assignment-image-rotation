#include "ToFromBMP.h"
#include "RotatePicture.h"
#include <stdio.h>
#include <stdlib.h>

#define READ_MODE "rb"
#define WRITE_MODE "wb"
#define EXPECTED_ARG 3

FILE* openFile(const char* filename, const char* mode);
void closeFile(FILE* file);

int main(int argc, char** argv) {
    if (argc == EXPECTED_ARG){
 struct image img = { .width = 0, .height = 0 };
        
        FILE* file1 = openFile(argv[1], READ_MODE);
        FILE* file2 = openFile(argv[2], WRITE_MODE);

        if (from_bmp(file1, &img) == READ_OK) {
            struct image imgRotated = rotate(img);
            free(img.data);
            if (to_bmp(file2, &imgRotated) == WRITE_OK) {
                free(imgRotated.data);
                printf("Success");
            }
            else{
                fprintf(stderr, "fail to");
            }
        }
        else{
            fprintf(stderr, "fail from");
        }

        closeFile(file1);
        closeFile(file2);
    }

        return 0;
}

FILE* openFile(const char* filename, const char* mode) {
    FILE* file = fopen(filename, mode);
    if (file==NULL){
        exit(EXIT_FAILURE);
    }
    return file;
}

void closeFile(FILE* file) {
    if (fclose(file) != 0) {
        exit(EXIT_FAILURE);
    }
}
