#include "Image.h"
#include "BMPHeader.h"
#include "ToFromBMP.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define PADDING 8
#define SIZE_PIXEL sizeof(struct pixel)
#define BYTE_ALIGMENT 4
#define SIZE_BMPHEADER sizeof(struct bmp_header)
#define ZERO 0
#define BI_SIZE 40
#define BI_PLANES 1
#define BI_BIT_COUNT 24
#define BF_TYPE 0x4d42


uint8_t calculate_padding(uint16_t w){
    uint32_t rowSize = (w * SIZE_PIXEL);
    return (uint8_t) (BYTE_ALIGMENT - (rowSize % BYTE_ALIGMENT)) % BYTE_ALIGMENT;
}

enum read_status from_bmp(FILE* in, struct image* img)
{

    struct bmp_header bmp_header_local;

    if (in == NULL){
        return READ_INVALID_SIGNATURE;
    }

    size_t bytesRead = fread(&bmp_header_local, SIZE_BMPHEADER, 1, in);
    if (bytesRead != 1){
        return READ_INVALID_HEADER;
    }

    img->height = bmp_header_local.biHeight;
    img->width = bmp_header_local.biWidth;

    uint8_t paddingSize = calculate_padding(img->width);

    if (bmp_header_local.bOffBits < SIZE_BMPHEADER) {
        return READ_INVALID_HEADER;
    }

    img->data = (struct pixel*)malloc(SIZE_PIXEL * img->width * img->height);

    if (img->data == NULL){
        return READ_INVALID_BITS;
    }

    if (fseek(in, bmp_header_local.bOffBits, SEEK_SET) != 0) {
        free(img->data);
        return READ_INVALID_BITS;
    }

    for (uint16_t y = 0; y < img->height; y++){
        bytesRead = fread(&(img->data[y * img->width]), SIZE_PIXEL, img->width, in);
        if (bytesRead != img->width) {
            free(img->data);
            return READ_INVALID_BITS;
        }
        fseek(in, paddingSize, SEEK_CUR);
    }

    return READ_OK;
}

enum write_status to_bmp(FILE* out, struct image* img){

    uint8_t paddingSize = calculate_padding(img->width);

    struct bmp_header new_header = {
            .biWidth = img->width,
            .biHeight = img->height,
            .bfileSize = (img -> width * img -> height * SIZE_PIXEL + img -> height * paddingSize) * sizeof(struct bmp_header),
            .biSizeImage = (img -> width * img -> height * SIZE_PIXEL + img -> height * paddingSize),
            .bfType = BF_TYPE,
            .bfReserved = ZERO,
            .bOffBits = SIZE_BMPHEADER,
            .biSize = BI_SIZE,
            .biPlanes = BI_PLANES,
            .biBitCount = BI_BIT_COUNT,
            .biCompression = ZERO,
            .biXPelsPerMeter = ZERO,
            .biYPelsPerMeter = ZERO,
            .biClrUsed = ZERO,
            .biClrImportant = ZERO
    };

    if (out == NULL){
        return WRITE_ERROR;
    }

    if (fwrite(&new_header, SIZE_BMPHEADER, 1, out) != 1) {
        return WRITE_HEADER_ERROR;
    }

    for (uint16_t y = 0; y < img->height; y++){
        if (fwrite(&(img->data[y * img->width]), SIZE_PIXEL, img->width, out) != img->width) {
            return WRITE_IMAGE_ERROR;
        }

        for (uint32_t i = 0; i < paddingSize; i++){
            uint8_t padding = PADDING;
            if (fwrite(&padding, sizeof(uint8_t), 1, out) != 1) {
                return WRITE_PADDING_ERROR;
            }
        }
    }

    return WRITE_OK;
}
