#ifndef DESERIALIZE_FROM_BMP_H
#define DESERIALIZE_FROM_BMP_H

#include "Image.h"
#include <stdio.h>

enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
};

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR,
    WRITE_HEADER_ERROR,
    WRITE_IMAGE_ERROR,
    WRITE_PADDING_ERROR
};

enum write_status to_bmp(FILE* out, struct image* img);

enum read_status from_bmp(FILE* in, struct image* img);

uint8_t calculate_padding(uint16_t w);

#endif
